#! /bin/bash


composer global require "laravel/installer=~1.1"

chmod -R 755 /var/www/html/hotelapp/storage 

chown -R www-data:www-data /var/www/html/hotelapp/storage

chown -R www-data:www-data /var/www/html/hoteapp/storage/logs/

cat > /etc/apache2/sites-available/000-default.conf <<EOL
<VirtualHost *:80>
    ServerName example.com
    DocumentRoot /var/www/html/hotelapp/public

    <Directory /var/www/html/hotelapp/public>
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
EOL
ln -s /etc/apache2/sites-available/example.com.conf /etc/apache2/sites-enabled/



sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php7.4/apache2/php.ini
/usr/sbin/apache2ctl -D FOREGROUND